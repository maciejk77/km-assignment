KM Assignment
==============

**Instructions:**

The purpose of this assessment is to complete a simple programming assignment.
You are expected to work on this task on your own, without help or advice from others. 
If you need clarification on any aspect of the assessment, please seek help from your organiser.

Please complete the coding assignment within a maximum of 24 hours.


**Coding Assignment:**

Create a responsive login/registration page like the image example 'login-page.png'.
Write a login/registration page in HTML5/CSS & JavaScript.
The JavaScript should be well structured, suitably commented.

**You are required to:**

- Use the provided index.html to build the login page
- Use the style.css to do the styling
- Use the script.js to do the email and password validation

**HTML & CSS:**

The HTML & CSS code should be valid and has to meet the W3C standard.

**JS:**

The form will need a validation before a user can login.
In order to do the validation complete the JavaScript.
Add an error message if the email or password are not correct.